import logo from './logo.svg';
import './App.css';
import {Component} from "react";

class App extends Component {
    state = {
        meals: []
    };

    async componentDidMount() {
        const response = await fetch('/findAll');
        const body = await response.json();
        this.setState({meals: body});
    }

    render() {
        const {meals} = this.state
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <div className="App-intro">
                        <h2>Meals</h2>
                        {meals.map(meal =>
                            <div key={meal.mealid}>
                                {meal.mealName} -{meal.mealType}-
                            </div>
                        )}
                    </div>
                </header>
            </div>
        );
    }
}

export default App;
