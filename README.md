# MealsApp

MealsApp is simple app to manage your daily meals.

- Create your own meals to eat

- Manage when a meal can appear

- History of meals you have already eaten

## URL
[MealsApp](http://mealsappdemo.com)

## Front repository
[Front](https://gitlab.com/victor.95.manuel/MealsApp-front)
