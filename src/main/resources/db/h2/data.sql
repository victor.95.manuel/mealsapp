INSERT INTO meal (mealid, mealname, mealtype, mealperiodicity, meallastday, discarded, username) VALUES
('e00b84fc-47d1-4ad9-9142-ea8844dd8585', 'Macarrones', 'LUNCH', 3, DATE '2021-06-16', '0', 'mealTest'),
('39739222-02c9-4a04-93b7-c7f18cf6ecaa', 'Pollo asado', 'LUNCH', 2, DATE '2021-06-11', '0', 'mealTest'),
('c049b162-da20-402f-b2d5-ed783c4b855b', 'Lentejas', 'LUNCH', 3, DATE '2021-06-09', '0', 'mealTest'),
('7d1c4913-bfcf-4d13-8b1b-fbbb7439b6c0', STRINGDECODE('Alb\u00f3ndigas'), 'LUNCH', 1, DATE '2021-06-12', '0', 'mealTest'),
('346c97ed-fbf3-4375-9123-7e547e86e697', 'Ensalada de pasta', 'LUNCH', 2, DATE '2021-06-16', '0', 'mealTest'),
('6a899f31-efc1-4d14-84df-ce9133e2ffcb', 'Pollo empanado', 'DINNER', 1, DATE '2021-06-16', '0', 'mealTest'),
('a933f5cd-dd5b-40fc-b147-e85ba11834d3', 'Pescado', 'DINNER', 2, DATE '2021-06-16', '0', 'mealTest'),
('03465240-1bb1-46c7-ab99-6fb7bdfd0ca8', 'Tortilla', 'DINNER', 2, DATE '2021-06-16', '0', 'mealTest'),
('402fd9fe-cd33-449c-b6f7-3c1f6d8b4860', STRINGDECODE('Guisantes con jam\u00f3n'), 'DINNER', 1, DATE '2021-06-16', '0', 'mealTest'),
('5e36b3b0-9a4a-4121-a7da-20de749def51', 'Sandwich Mixto', 'DINNER', 1, NULL, '0', 'mealTest'),
('f7331278-82cd-4f79-b924-f868f463080b', 'Chuletas', 'DINNER', 2, NULL, '0', 'mealTest'),
('6d06df4e-84fd-4780-9534-ec7e2100c7e2', 'Cocido', 'LUNCH', 4, DATE '2021-06-16', '0', 'mealTest');

INSERT INTO meal_history VALUES
('6d06df4e-84fd-4780-9534-ec7e2100c7e2', DATE '2021-06-11', 'Cocido', 'mealTest'),
('346c97ed-fbf3-4375-9123-7e547e86e697', DATE '2021-06-14', 'Ensalada de pasta', 'mealTest'),
('e00b84fc-47d1-4ad9-9142-ea8844dd8585', DATE '2021-06-09', 'Macarrones', 'mealTest'),
('39739222-02c9-4a04-93b7-c7f18cf6ecaa', DATE '2021-06-13', 'Pollo asado', 'mealTest'),
('7d1c4913-bfcf-4d13-8b1b-fbbb7439b6c0', DATE '2021-06-10', 'Albóndigas', 'mealTest'),
('c049b162-da20-402f-b2d5-ed783c4b855b', DATE '2021-06-09', 'Lentejas', 'mealTest'),
('6a899f31-efc1-4d14-84df-ce9133e2ffcb', DATE '2021-06-12', 'Pollo empanado', 'mealTest'),
('a933f5cd-dd5b-40fc-b147-e85ba11834d3', DATE '2021-06-09', 'Pescado', 'mealTest'),
('03465240-1bb1-46c7-ab99-6fb7bdfd0ca8', DATE '2021-06-14', 'Tortilla', 'mealTest'),
('402fd9fe-cd33-449c-b6f7-3c1f6d8b4860', DATE '2021-06-13', 'Guisantes con jamón', 'mealTest'),
('5e36b3b0-9a4a-4121-a7da-20de749def51', DATE '2021-06-12', 'Sandwich Mixto', 'mealTest'),
('f7331278-82cd-4f79-b924-f868f463080b', DATE '2021-06-09', 'Chuletas', 'mealTest'),
('e00b84fc-47d1-4ad9-9142-ea8844dd8585', DATE '2021-06-16', 'Macarrones', 'mealTest'),
('6d06df4e-84fd-4780-9534-ec7e2100c7e2', DATE '2021-06-16', 'Cocido', 'mealTest'),
('6a899f31-efc1-4d14-84df-ce9133e2ffcb', DATE '2021-06-16', 'Pollo empanado', 'mealTest'),
('a933f5cd-dd5b-40fc-b147-e85ba11834d3', DATE '2021-06-16', 'Pescado', 'mealTest'),
('03465240-1bb1-46c7-ab99-6fb7bdfd0ca8', DATE '2021-06-16', 'Tortilla', 'mealTest'),
('402fd9fe-cd33-449c-b6f7-3c1f6d8b4860', DATE '2021-06-16', 'Guisantes con jamón', 'mealTest'),
('346c97ed-fbf3-4375-9123-7e547e86e697', DATE '2021-06-16', 'Ensalada de pasta', 'mealTest');

INSERT INTO meal_user VALUES
('e00b84fc-47d1-4ad9-9142-ea8844dd8581', 'mealTest', '1'),
('e00b84fc-47d1-4ad9-9142-7e547e86e693', 'guest', '1');