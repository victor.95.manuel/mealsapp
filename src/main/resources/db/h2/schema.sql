DROP TABLE IF EXISTS meal;

CREATE TABLE meal
(
    mealid varchar(255),
    mealname varchar(255),
    mealtype varchar2(255),
    mealperiodicity number,
    meallastday date,
    discarded varchar(1),
    username varchar2(255),

    PRIMARY KEY (mealid, username)
);

DROP TABLE IF EXISTS meal_history;

CREATE TABLE meal_history
(
    mealid varchar(255),
    meallastday date,
    mealname varchar2(255),
    username varchar2(255),

    PRIMARY KEY (mealid, meallastday, username)
);

DROP TABLE IF EXISTS meal_user;

CREATE TABLE meal_user
(
    userid varchar(255),
    username varchar(255),
    activated number,

    PRIMARY KEY (userid)
);