package com.mealsapp.mealsservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MealsServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MealsServiceApplication.class, args);
    }

}
