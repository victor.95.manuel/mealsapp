package com.mealsapp.mealsservice.infrastructure;

import com.mealsapp.mealsservice.domain.DomainError;

public class NotFoundUserName extends DomainError {

    public NotFoundUserName(String classname) {
        super("User not found", classname);
    }

}
