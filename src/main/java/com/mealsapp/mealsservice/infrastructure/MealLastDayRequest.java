package com.mealsapp.mealsservice.infrastructure;

public class MealLastDayRequest {

    public String meallastday;
    public String username;

    public MealLastDayRequest(String meallastday, String username) {
        this.meallastday = meallastday;
        this.username = username;
    }

    public String getMeallastday() {
        return meallastday;
    }

    public String getUsername() {
        return username;
    }
}
