package com.mealsapp.mealsservice.infrastructure;

public class MealUserRequest {

    public String username;

    public MealUserRequest(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }
}
