package com.mealsapp.mealsservice.infrastructure;

public class MealNameRequest {

    public final String mealname;
    public final String username;

    public MealNameRequest(String mealname, String username) {
        this.mealname = mealname;
        this.username = username;
    }

    public String getMealname() {
        return mealname;
    }

    public String getUsername() {
        return username;
    }
}
