package com.mealsapp.mealsservice.infrastructure;


import java.util.List;

public class MealPageableResponse {

    private final List<MealResponse> pageableList;
    private final int pagesNumber;
    private final int currentPageNumber;

    public MealPageableResponse(List<MealResponse> pageableList, int pagesNumber, int currentPageNumber) {
        this.pageableList = pageableList;
        this.pagesNumber = pagesNumber;
        this.currentPageNumber = currentPageNumber;
    }

    public List<MealResponse> getPageableList() {
        return pageableList;
    }

    public int getPagesNumber() {
        return pagesNumber;
    }

    public int getCurrentPageNumber() {
        return currentPageNumber;
    }
}
