package com.mealsapp.mealsservice.infrastructure;

public class MealRemoveRequest {

    public String mealid;
    public String username;

    public MealRemoveRequest(String mealid, String username) {
        this.mealid = mealid;
        this.username = username;
    }

    public String getMealid() {
        return mealid;
    }

    public String getUsername() {
        return username;
    }
}
