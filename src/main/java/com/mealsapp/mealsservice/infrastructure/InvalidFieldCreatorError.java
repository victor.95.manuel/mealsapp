package com.mealsapp.mealsservice.infrastructure;

public class InvalidFieldCreatorError {

    private final String fieldName;
    private final String error;

    public InvalidFieldCreatorError(String fieldName, String error) {
        this.fieldName = fieldName;
        this.error = error;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getError() {
        return error;
    }
}
