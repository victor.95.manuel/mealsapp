package com.mealsapp.mealsservice.infrastructure;

import java.util.Date;

public class MealResponse {

    public String mealid;
    public String mealName;
    public String mealType;
    public int mealPeriodicity;
    public Date mealLastDay;
    public String discarded;
    public String username;

    public MealResponse(String mealid, String mealName, String mealType, int mealPeriodicity, Date mealLastDay, String discarded, String username) {
        this.mealid = mealid;
        this.mealName = mealName;
        this.mealType = mealType;
        this.mealPeriodicity = mealPeriodicity;
        this.mealLastDay = mealLastDay;
        this.discarded = discarded;
        this.username = username;
    }

    public MealResponse(String mealid, String mealName, Date mealLastDay, String username) {
        this.mealid = mealid;
        this.mealName = mealName;
        this.mealLastDay = mealLastDay;
        this.username = username;
    }

    public String getMealid() {
        return mealid;
    }

    public String getMealName() {
        return mealName;
    }

    public String getMealType() {
        return mealType;
    }

    public int getMealPeriodicity() {
        return mealPeriodicity;
    }

    public Date getMealLastDay() {
        return mealLastDay;
    }

    public String getDiscarded() {
        return discarded;
    }

    public String getUsername() {
        return username;
    }
}
