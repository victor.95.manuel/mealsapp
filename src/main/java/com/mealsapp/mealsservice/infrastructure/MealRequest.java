package com.mealsapp.mealsservice.infrastructure;

import com.mealsapp.mealsservice.domain.MealUserName;

public class MealRequest {

    public String mealId;
    public String mealName;
    public String mealType;
    public int mealPeriodicity;
    public String mealLastDay;
    public String username;

    public MealRequest() {
    }

    public MealRequest(String mealId, String mealName, String mealType, int mealPeriodicity, String mealLastDay, String username) {
        this.mealId = mealId;
        this.mealName = mealName;
        this.mealType = mealType;
        this.mealPeriodicity = mealPeriodicity;
        this.mealLastDay = mealLastDay;
        this.username = username;
    }

    public MealRequest(String mealName, String mealType, int mealPeriodicity, String mealLastDay, String username) {
        this.mealName = mealName;
        this.mealType = mealType;
        this.mealPeriodicity = mealPeriodicity;
        this.mealLastDay = mealLastDay;
        this.username = username;
    }

    public MealRequest(String mealName, String mealType, int mealPeriodicity, String username) {
        this.mealName = mealName;
        this.mealType = mealType;
        this.mealPeriodicity = mealPeriodicity;
        this.username = username;
    }

    public MealRequest(String mealName, String mealType, String username) {
        this.mealName = mealName;
        this.mealType = mealType;
        this.username = username;
    }

    public MealRequest(String mealName, String username) {
        this.mealName = mealName;
        this.username = username;
    }

    public String getMealId() {
        return mealId;
    }

    public String getMealName() {
        return mealName;
    }

    public String getMealType() {
        return mealType;
    }

    public int getMealPeriodicity() {
        return mealPeriodicity;
    }

    public String getMealLastDay() {
        return mealLastDay;
    }

    public String getUsername() {
        return username;
    }
}
