package com.mealsapp.mealsservice.infrastructure;

import com.mealsapp.mealsservice.application.MealCreator;
import com.mealsapp.mealsservice.application.MealFinder;
import com.mealsapp.mealsservice.application.MealLogin;
import com.mealsapp.mealsservice.domain.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class MealLoginController {

    private final MealLogin mealLogin;

    public MealLoginController(MealLogin mealLogin) {
        this.mealLogin = mealLogin;
    }

    @GetMapping("/findUserName")
    public ResponseEntity<?> loginUser(@RequestParam MealLoginRequest mealLoginRequest) {
        List<Result> resultList = new ArrayList<>();

        Result mealUserName = MealUserName.parseMealUser(mealLoginRequest.getUsername());
        resultList.add(mealUserName);

        ResultList errorList = new ResultList(resultList);

        if (errorList.hasAnyError()) {
            errorList.printErrors();
            return ResponseEntity.badRequest().body(errorList.getContent());
        }

        MealLoginResponse userFound = mealLogin.findUser(mealLoginRequest);

        if (userFound == null) {
            List<Result> loginList = new ArrayList<>();
            Result mealLoginUser = Result.error(new NotFoundUserName(MealLogin.class.getSimpleName()));
            loginList.add(mealLoginUser);
            ResultList errorLoginList = new ResultList(loginList);
            errorLoginList.printErrors();
            return ResponseEntity.badRequest().body(errorLoginList.getContent());
        }

        return ResponseEntity.ok().body(new ArrayList<>(Arrays.asList(new Result(new LoginSuccess()))));
    }
}
