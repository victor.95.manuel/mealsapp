package com.mealsapp.mealsservice.infrastructure;

public class MealLoginResponse {

    public String username;
    public int activated;

    public MealLoginResponse(String username, int activated) {
        this.username = username;
        this.activated = activated;
    }

    public String getUsername() {
        return username;
    }

    public int getActivated() {
        return activated;
    }
}
