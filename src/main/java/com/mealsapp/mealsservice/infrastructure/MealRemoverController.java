package com.mealsapp.mealsservice.infrastructure;

import com.mealsapp.mealsservice.application.MealRemover;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MealRemoverController {

    MealRemover mealRemover;

    public MealRemoverController(MealRemover mealRemover) {
        this.mealRemover = mealRemover;
    }

    @PostMapping("/removeMeal")
    public void deleteMeal(@RequestBody MealRemoveRequest mealRemoveRequest) {
        mealRemover.remove(mealRemoveRequest);
    }

}
