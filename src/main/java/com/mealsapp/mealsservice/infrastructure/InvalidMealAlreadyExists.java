package com.mealsapp.mealsservice.infrastructure;

import com.mealsapp.mealsservice.domain.DomainError;

public class InvalidMealAlreadyExists extends DomainError {

    public InvalidMealAlreadyExists(String classname) {
        super("Meal already exists", classname);
    }

}
