package com.mealsapp.mealsservice.infrastructure;

import com.mealsapp.mealsservice.application.MealFinder;
import com.mealsapp.mealsservice.application.MealLogin;
import com.mealsapp.mealsservice.domain.*;
import org.springframework.data.domain.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class MealFinderController {

    private final String LUNCH = "LUNCH";
    private final String DINNER = "DINNER";

    private final MealFinder mealFinder;

    public MealFinderController(MealFinder mealFinder) {
        this.mealFinder = mealFinder;
    }

    @GetMapping("/findAll")
    public ResponseEntity<List<MealResponse>> findMeals(@RequestParam String username) {
        return ResponseEntity.ok().body(mealFinder.findAllMeals(username));
    }

    @GetMapping("/findAllPageable")
    public ResponseEntity<MealPageableResponse> findPageableMeals(@RequestParam int page, @RequestParam int size, @RequestParam String username) {
        List<MealResponse> allMeals = mealFinder.findAllMeals(username)
                .stream()
                .filter(meal -> meal.getUsername().equals(username))
                .sorted(Comparator.comparing(MealResponse::getMealType).reversed()
                        .thenComparing(MealResponse::getMealLastDay, Comparator.nullsLast(Comparator.reverseOrder()))
                        .thenComparing(MealResponse::getMealName))
                .collect(Collectors.toList());

        double allMealsCount = allMeals.size();
        int pagesNumber = (int) Math.ceil(allMealsCount / (double) size);

        Pageable paging = PageRequest.of(page, size);
        int start = (int) paging.getOffset();
        int end = Math.min((start + paging.getPageSize()), allMeals.size());

        return ResponseEntity
                .ok()
                .body(new MealPageableResponse(
                        allMeals.subList(start, end),
                        pagesNumber,
                        paging.getPageNumber()
                ));

    }

    @GetMapping("/findHistoryMealsPageable")
    public ResponseEntity<MealPageableResponse> findHistoryMealsPageable(@RequestParam int page, @RequestParam int size, @RequestParam String username) {
        List<MealResponse> historyMealsPageable = mealFinder.findHistoryMealsPageable(username)
                .stream()
                .filter(meal -> meal.getUsername().equals(username))
                .sorted(Comparator.comparing(MealResponse::getMealLastDay).reversed().thenComparing(MealResponse::getMealName))
                .collect(Collectors.toList());

        double historyMealsCount = historyMealsPageable.size();
        int pagesNumber = (int) Math.ceil(historyMealsCount / (double) size);

        Pageable paging = PageRequest.of(page, size);
        int start = (int) paging.getOffset();
        int end = Math.min((start + paging.getPageSize()), historyMealsPageable.size());

        return ResponseEntity
                .ok()
                .body(new MealPageableResponse(
                        historyMealsPageable.subList(start, end),
                        pagesNumber,
                        paging.getPageNumber()
                ));
    }

    @GetMapping("/findMealByNamePageable")
    public ResponseEntity<MealPageableResponse> findMealByNamePageable(@RequestParam int page,
                                                                       @RequestParam int size,
                                                                       @RequestParam String mealName,
                                                                       @RequestParam String username) {
        List<MealResponse> mealsByName = mealFinder.findMealByName(new MealRequest(mealName, username));

        double mealsByNameCount = mealsByName.size();
        int pagesNumber = (int) Math.ceil(mealsByNameCount / (double) size);

        Pageable paging = PageRequest.of(page, size);
        int start = (int) paging.getOffset();
        int end = Math.min((start + paging.getPageSize()), mealsByName.size());

        return ResponseEntity
                .ok()
                .body(new MealPageableResponse(
                        mealsByName.subList(start, end),
                        pagesNumber,
                        paging.getPageNumber()
                ));
    }

    @GetMapping("/findMealByTypePageable")
    public ResponseEntity<MealPageableResponse> findMealByTypePageable(@RequestParam int page, @RequestParam int size, @RequestParam String mealType) {
        List<MealResponse> mealsByType = mealFinder.findMealsByType(new MealTypeRequest(mealType));

        double mealsByTypeCount = mealsByType.size();
        double sizeToDivide = size;
        int pagesNumber = (int) Math.ceil(mealsByTypeCount / sizeToDivide);

        Pageable paging = PageRequest.of(page, size);
        int start = (int) paging.getOffset();
        int end = Math.min((start + paging.getPageSize()), mealsByType.size());

        return ResponseEntity
                .ok()
                .body(new MealPageableResponse(
                        mealsByType.subList(start, end),
                        pagesNumber,
                        paging.getPageNumber()
                ));
    }

    @GetMapping("/findMealByNameAndTypePageable")
    public ResponseEntity<MealPageableResponse> findMealByNameAndTypePageable(@RequestParam int page,
                                                                              @RequestParam int size,
                                                                              @RequestParam String mealName,
                                                                              @RequestParam String mealType,
                                                                              @RequestParam String username) {
        List<MealResponse> mealsByNameAndType = mealFinder.findMealByNameAndTypePageable(new MealRequest(mealName, username), new MealTypeRequest(mealType));

        double mealsByTypeCount = mealsByNameAndType.size();
        double sizeToDivide = size;
        int pagesNumber = (int) Math.ceil(mealsByTypeCount / sizeToDivide);

        Pageable paging = PageRequest.of(page, size);
        int start = (int) paging.getOffset();
        int end = Math.min((start + paging.getPageSize()), mealsByNameAndType.size());

        return ResponseEntity
                .ok()
                .body(new MealPageableResponse(
                        mealsByNameAndType.subList(start, end),
                        pagesNumber,
                        paging.getPageNumber()
                ));
    }

    @GetMapping("/findHistoryMealByNameAndLastDayPageable")
    public ResponseEntity<MealPageableResponse> findHistoryMealByNameAndLastDayPageable(@RequestParam int page,
                                                                                        @RequestParam int size,
                                                                                        @RequestParam String mealName,
                                                                                        @RequestParam String mealLastDay,
                                                                                        @RequestParam String username) {

        List<MealResponse> historyMealsByNameAndLastDay = mealFinder.findHistoryMealByNameAndLastDayPageable(
                        new MealRequest(mealName, username),
                        new MealLastDayRequest(mealLastDay, username)
                )
                .stream()
                .sorted(Comparator.comparing(MealResponse::getMealLastDay).reversed().thenComparing(MealResponse::getMealName))
                .collect(Collectors.toList());

        double historyMealsCount = historyMealsByNameAndLastDay.size();
        int pagesNumber = (int) Math.ceil(historyMealsCount / (double) size);

        Pageable paging = PageRequest.of(page, size);
        int start = (int) paging.getOffset();
        int end = Math.min((start + paging.getPageSize()), historyMealsByNameAndLastDay.size());

        return ResponseEntity
                .ok()
                .body(new MealPageableResponse(
                        historyMealsByNameAndLastDay.subList(start, end),
                        pagesNumber,
                        paging.getPageNumber()
                ));
    }

    @GetMapping("/findMealByNameAndTypeAndLastDayPageable")
    public ResponseEntity<MealPageableResponse> findMealByNameAndTypeAndLastDayPageable(@RequestParam int page,
                                                                                        @RequestParam int size,
                                                                                        @RequestParam String mealName,
                                                                                        @RequestParam String mealType,
                                                                                        @RequestParam String mealLastDay,
                                                                                        @RequestParam boolean emptyCheckBox,
                                                                                        @RequestParam String username) {

        List<MealResponse> mealsByNameAndTypeAndLastDay = mealFinder.findMealByNameAndTypeAndLastDayPageable(
                        new MealRequest(mealName, username),
                        new MealTypeRequest(mealType.toUpperCase(), username),
                        new MealLastDayRequest(mealLastDay, username),
                        emptyCheckBox
                ).stream()
                .sorted(Comparator.comparing(MealResponse::getMealType).reversed().thenComparing(MealResponse::getMealName))
                .collect(Collectors.toList());

        double mealsCount = mealsByNameAndTypeAndLastDay.size();
        double sizeToDivide = size;
        int pagesNumber = (int) Math.ceil(mealsCount / sizeToDivide);

        Pageable paging = PageRequest.of(page, size);
        int start = (int) paging.getOffset();
        int end = Math.min((start + paging.getPageSize()), mealsByNameAndTypeAndLastDay.size());

        return ResponseEntity
                .ok()
                .body(new MealPageableResponse(
                        mealsByNameAndTypeAndLastDay.subList(start, end),
                        pagesNumber,
                        paging.getPageNumber()
                ));
    }

    @GetMapping("/findByType")
    public ResponseEntity<List<MealResponse>> findMealsByType(@RequestBody MealTypeRequest mealTypeRequest) {
        return ResponseEntity.ok().body(mealFinder.findMealsByType(mealTypeRequest));
    }

    @GetMapping("/findMealToEat")
    public ResponseEntity<?> findMealToEat(@RequestParam String mealTypeRequest, @RequestParam String username) {
        List<Result> resultList = new ArrayList<>();

        Result mealType = MealType.parseMealType(mapToMealType(mealTypeRequest).getValue());
        resultList.add(mealType);

        ResultList errorList = new ResultList(resultList);

        if (errorList.hasAnyError()) {
            errorList.printErrors();
            return ResponseEntity.badRequest().body(errorList.getContent());
        }

        MealResponse notEatenMeal = mealFinder.findNotEatenMeal(new MealTypeRequest(mealTypeRequest.toUpperCase(), username));

        if (notEatenMeal == null)
        {
            List<Result> notEatenMealList = new ArrayList<>();
            Result notEatenMealResult = Result.error(new NotFoundMealToEat(Meal.class.getSimpleName()));
            notEatenMealList.add(notEatenMealResult);
            ResultList errorNotEatenMeal = new ResultList(notEatenMealList);
            errorNotEatenMeal.printErrors();
            return ResponseEntity.badRequest().body(errorNotEatenMeal.getContent());
        }

        return ResponseEntity.ok().body(notEatenMeal);
    }

    @GetMapping("/findMealsByDate")
    public ResponseEntity<List<MealResponse>> findMealsByDate(@RequestBody MealLastDayRequest mealLastDayRequest) {
        return ResponseEntity.ok().body(mealFinder.findMealsByDate(mealLastDayRequest));
    }

    @GetMapping("/findDatesOfMeal")
    public ResponseEntity<List<MealResponse>> findDatesOfMeal(@RequestBody MealRequest mealRequest) {
        return ResponseEntity.ok().body(mealFinder.findDatesOfMeal(mealRequest));
    }

    private MealType mapToMealType(String mealType) {
        if (mealType == null) return new MealType(null);

        switch (mealType.toUpperCase().trim()) {
            case LUNCH:
                return new MealType(MealType.MealTypeValue.LUNCH);
            case DINNER:
                return new MealType(MealType.MealTypeValue.DINNER);
            default:
                return new MealType(null);
        }
    }
}
