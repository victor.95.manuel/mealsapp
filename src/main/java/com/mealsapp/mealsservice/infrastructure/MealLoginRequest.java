package com.mealsapp.mealsservice.infrastructure;

public class MealLoginRequest {

    public String username;

    public MealLoginRequest() {
    }

    public MealLoginRequest(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }
}
