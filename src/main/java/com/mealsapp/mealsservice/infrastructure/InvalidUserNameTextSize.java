package com.mealsapp.mealsservice.infrastructure;

import com.mealsapp.mealsservice.domain.DomainError;

public class InvalidUserNameTextSize extends DomainError {

    public InvalidUserNameTextSize(String classname) {
        super("Invalid username text size", classname);
    }

}
