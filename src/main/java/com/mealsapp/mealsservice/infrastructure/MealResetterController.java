package com.mealsapp.mealsservice.infrastructure;

import com.mealsapp.mealsservice.application.MealResetter;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MealResetterController {

    private final MealResetter mealResetter;

    public MealResetterController(MealResetter mealResetter) {
        this.mealResetter = mealResetter;
    }

    @PostMapping("/resetMeal")
    public void resetMeal(@RequestBody MealResetRequest mealResetRequest) {
        mealResetter.resetMeal(mealResetRequest);
    }
}
