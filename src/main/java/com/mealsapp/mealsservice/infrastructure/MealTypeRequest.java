package com.mealsapp.mealsservice.infrastructure;

public class MealTypeRequest {

    public String mealType;
    public String username;

    public MealTypeRequest(String mealType) {
        this.mealType = mealType;
    }

    public MealTypeRequest(String mealType, String username) {
        this.mealType = mealType;
        this.username = username;
    }

    public String getMealType() {
        return mealType;
    }

    public String getUsername() {
        return username;
    }
}
