package com.mealsapp.mealsservice.infrastructure;

import com.mealsapp.mealsservice.domain.Result;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class ResultList {

    private List<Result> content = Collections.emptyList();

    public ResultList(List<Result> content) {
        this.content = content;
    }

    public List<Result> getContent() {
        return content;
    }

    public Boolean hasAnyError() {
        return content.stream().anyMatch(Result::hasError);
    }

    public void printErrors() {
        content.stream().filter(Result::hasError).forEach(Result::printError);
    }
}
