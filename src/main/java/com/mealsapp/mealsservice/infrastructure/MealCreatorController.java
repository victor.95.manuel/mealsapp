package com.mealsapp.mealsservice.infrastructure;

import com.mealsapp.mealsservice.application.MealCreator;
import com.mealsapp.mealsservice.application.MealFinder;
import com.mealsapp.mealsservice.domain.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class MealCreatorController {

    private final String LUNCH = "LUNCH";
    private final String DINNER = "DINNER";

    private final MealCreator mealCreator;
    private final MealFinder mealFinder;

    public MealCreatorController(MealCreator mealCreator, MealFinder mealFinder) {
        this.mealCreator = mealCreator;
        this.mealFinder = mealFinder;
    }

    @PostMapping("/meal")
    public ResponseEntity<?> createMeal(@RequestBody MealRequest mealRequest) {
        List<Result> resultList = new ArrayList<>();
        ResultList errorList;

        Result mealName = MealName.parseMealName(mealRequest.getMealName());
        resultList.add(mealName);

        Result mealType = MealType.parseMealType(mapToMealType(mealRequest.getMealType()).getValue());
        resultList.add(mealType);

        Result mealPeriodicity = MealPeriodicity.parseMealPeriodicity(mealRequest.getMealPeriodicity());
        resultList.add(mealPeriodicity);

        Result mealUsername = MealUserName.parseMealUser(mealRequest.getUsername());
        resultList.add(mealUsername);

        errorList = new ResultList(resultList);

        if (!errorList.hasAnyError()) {
            Result checkIfMealIsAlreadyInserted = isMealAlreadyInserted(mealRequest);
            resultList.add(checkIfMealIsAlreadyInserted);
            errorList = new ResultList(resultList);
        }

        if (errorList.hasAnyError()) {
            errorList.printErrors();
            return ResponseEntity.badRequest().body(errorList.getContent());
        } else {
            Meal mealToSave = new Meal(
                    (MealName) mealName.getElement(),
                    (MealType) mealType.getElement(),
                    (MealPeriodicity) mealPeriodicity.getElement(),
                    (MealUserName) mealUsername.getElement()
            );

            mealCreator.create(mealToSave);
        }

        return ResponseEntity.ok().body(new ArrayList<>(Arrays.asList(new Result(new DomainSuccess()))));
    }

    private Result isMealAlreadyInserted(MealRequest mealRequest) {
        List<MealResponse> mealToInsert = mealFinder.findAllMeals(mealRequest.getUsername())
                .stream()
                .filter(meal -> meal.getMealName().toUpperCase().equals(mealRequest.getMealName().toUpperCase()))
                .filter(meal -> meal.getMealType().equals(mealRequest.getMealType()))
                .filter(meal -> meal.getUsername().equals(mealRequest.getUsername()))
                .collect(Collectors.toList());

        if (!mealToInsert.isEmpty()) return Result.error(new InvalidMealAlreadyExists(Meal.class.getSimpleName()));

        return Result.valid(new MealName(mealRequest.getMealName()));
    }

    @PostMapping("/updateMeal")
    public ResponseEntity<?> updateMeal(@RequestBody MealRequest mealRequest) {
        List<Result> resultList = new ArrayList<>();

        Result mealName = MealName.parseMealName(mealRequest.getMealName());
        resultList.add(mealName);

        Result mealType = MealType.parseMealType(mapToMealType(mealRequest.getMealType()).getValue());
        resultList.add(mealType);

        Result mealPeriodicity = MealPeriodicity.parseMealPeriodicity(mealRequest.getMealPeriodicity());
        resultList.add(mealPeriodicity);

        Result mealUsername = MealUserName.parseMealUser(mealRequest.getUsername());
        resultList.add(mealUsername);

        ResultList errorList = new ResultList(resultList);

        if (errorList.hasAnyError()) {
            errorList.printErrors();
            return ResponseEntity.badRequest().body(errorList.getContent());
        } else {
            Meal mealToSave = new Meal(
                    mealRequest.getMealId(),
                    (MealName) mealName.getElement(),
                    (MealType) mealType.getElement(),
                    (MealPeriodicity) mealPeriodicity.getElement(),
                    getMealLastDayFromRequest(mealRequest),
                    "0",
                    (MealUserName) mealUsername.getElement()
            );

            mealCreator.create(mealToSave);
        }

        return ResponseEntity.ok().body(new ArrayList<>(Arrays.asList(new Result(new DomainSuccess()))));
    }

    private MealType mapToMealType(String mealType) {
        if (mealType == null) return new MealType(null);

        switch (mealType.toUpperCase().trim()) {
            case LUNCH:
                return new MealType(MealType.MealTypeValue.LUNCH);
            case DINNER:
                return new MealType(MealType.MealTypeValue.DINNER);
            default:
                return new MealType(null);
        }
    }

    private MealLastDay getMealLastDayFromRequest(MealRequest mealRequest) {
        if (mealRequest.getMealLastDay() == null || mealRequest.getMealLastDay().length() == 0) return null;
        try {
            return new MealLastDay(new SimpleDateFormat("yyyy-MM-dd").parse(mealRequest.mealLastDay.substring(0, 10)));
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
}
