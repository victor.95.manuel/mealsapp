package com.mealsapp.mealsservice.infrastructure;

import com.mealsapp.mealsservice.application.MealDiscarder;
import com.mealsapp.mealsservice.domain.DomainSuccess;
import com.mealsapp.mealsservice.domain.Result;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;

@RestController
public class MealDiscarderController {

    private final MealDiscarder mealDiscarder;

    public MealDiscarderController(MealDiscarder mealDiscarder) {
        this.mealDiscarder = mealDiscarder;
    }

    @PostMapping("/discardMeal")
    public ResponseEntity<?> discardMeal(@RequestBody MealRequest mealToDiscard) {
        mealDiscarder.discardMeal(mealToDiscard);
        return ResponseEntity.ok().body(new ArrayList<>(Arrays.asList(new Result(new DomainSuccess()))));
    }

    @PostMapping("/cleanDiscardedMeal")
    public ResponseEntity<?> cleanDiscardedMeal(@RequestBody MealUserRequest username) {
        mealDiscarder.cleanDiscardedMeal(username);
        return ResponseEntity.ok().body(new ArrayList<>(Arrays.asList(new Result(new DomainSuccess()))));
    }
}
