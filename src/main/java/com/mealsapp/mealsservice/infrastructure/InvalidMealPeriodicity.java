package com.mealsapp.mealsservice.infrastructure;

import com.mealsapp.mealsservice.domain.DomainError;

public class InvalidMealPeriodicity extends DomainError {

    public InvalidMealPeriodicity(String classname) {
        super("Invalid meal periodicity", classname);
    }

}
