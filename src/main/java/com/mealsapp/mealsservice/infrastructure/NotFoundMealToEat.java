package com.mealsapp.mealsservice.infrastructure;

import com.mealsapp.mealsservice.domain.DomainError;

public class NotFoundMealToEat extends DomainError {

    public NotFoundMealToEat(String classname) {
        super("Meal not found", classname);
    }

}
