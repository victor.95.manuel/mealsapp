package com.mealsapp.mealsservice.infrastructure;

import com.mealsapp.mealsservice.domain.DomainError;

public class InvalidTextSize extends DomainError {

    public InvalidTextSize(String classname) {
        super("Invalid name text size", classname);
    }

}
