package com.mealsapp.mealsservice.infrastructure;

public class MealResetRequest {

    public String mealid;
    public String username;

    public MealResetRequest(String mealid, String username) {
        this.mealid = mealid;
        this.username = username;
    }

    public String getMealid() {
        return mealid;
    }

    public String getUsername() {
        return username;
    }
}
