package com.mealsapp.mealsservice.infrastructure;

import com.mealsapp.mealsservice.domain.DomainError;

public class InvalidMealType extends DomainError {

    public InvalidMealType(String classname) {
        super("Invalid meal type", classname);
    }

}
