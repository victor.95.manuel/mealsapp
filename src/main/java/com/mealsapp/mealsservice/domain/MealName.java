package com.mealsapp.mealsservice.domain;

import com.mealsapp.mealsservice.infrastructure.InvalidTextSize;

public class MealName {

    private static final int MIN_VALUE_LENGTH = 2;
    private static final int MAX_VALUE_LENGTH = 50;

    private final String value;

    public MealName(String value) {
        this.value = value.trim();
    }

    public static <T> Result parseMealName(String value) {
        if (value == null || value.length() < MIN_VALUE_LENGTH || value.length() > MAX_VALUE_LENGTH)
            return Result.error(new InvalidTextSize(MealName.class.getSimpleName()));

        return Result.valid(new MealName(value));
    }

    public String getValue() {
        return value;
    }
}
