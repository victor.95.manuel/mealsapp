package com.mealsapp.mealsservice.domain;

public class DomainError {

    private final String value;
    private final String className;

    public DomainError(String value, String className) {
        this.value = value;
        this.className = className;
    }

    public String getError() {
        return value;
    }

    public String getClassName() {
        return className;
    }
}
