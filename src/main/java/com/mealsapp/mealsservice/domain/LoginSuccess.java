package com.mealsapp.mealsservice.domain;

public class LoginSuccess {

    private final String className = "LoginSuccess";
    private final String error = "Success";

    public String getClassName() {
        return className;
    }

    public String getError() {
        return error;
    }
}
