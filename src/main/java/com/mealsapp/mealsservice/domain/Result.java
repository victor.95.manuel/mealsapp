package com.mealsapp.mealsservice.domain;

public class Result<T> {

    private final T element;

    public Result(T element) {
        this.element = element;
    }

    public T getElement() {
        return element;
    }

    public static <T> Result valid(T value) {
        return new Result(value);
    }

    public static Result<DomainError> error(DomainError error) {
        return new Result(error);
    }

    public static Result<DomainSuccess> success(DomainSuccess success) {
        return new Result(success);
    }

    public Boolean hasError() {
        return element instanceof DomainError;
    }

    public void printError() {
        if (hasError()) {
            DomainError error = (DomainError) this.element;
            System.out.println("Field: " + error.getClassName() + " -> Error: " + error.getError());
        } else {
            throw new IllegalArgumentException("Can't print error because the object is not an Error");
        }
    }
}
