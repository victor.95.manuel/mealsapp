package com.mealsapp.mealsservice.domain;

public class InvalidMealTypeException extends Exception {
    public InvalidMealTypeException(String errorMessage) {
        super(errorMessage);
    }
}
