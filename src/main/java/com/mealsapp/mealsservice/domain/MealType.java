package com.mealsapp.mealsservice.domain;

import com.mealsapp.mealsservice.infrastructure.InvalidMealType;
import com.mealsapp.mealsservice.infrastructure.InvalidTextSize;

import java.util.Arrays;
import java.util.stream.Collectors;

public class MealType {

    public enum MealTypeValue {
        LUNCH, DINNER
    }

    private final MealTypeValue value;

    public MealType(MealTypeValue value) {
        this.value = value;
    }

    public static <T> Result parseMealType(MealTypeValue value) {
        if (!Arrays.stream(MealTypeValue.values()).collect(Collectors.toList()).contains(value))
            return Result.error(new InvalidMealType(MealType.class.getSimpleName()));

        return Result.valid(new MealType(value));
    }

    public MealTypeValue getValue() {
        return value;
    }
}
