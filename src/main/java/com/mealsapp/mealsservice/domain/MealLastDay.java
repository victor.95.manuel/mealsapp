package com.mealsapp.mealsservice.domain;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public class MealLastDay {

    private final Date value;

    public MealLastDay(Date value) {
        doValidation(value);
        this.value = value;
    }

    private void doValidation(Date value) {

    }

    public int getAmountOfDaysFromLastEatenDay() {
        LocalDateTime currentDate = LocalDateTime.now().truncatedTo(ChronoUnit.DAYS);
        LocalDateTime mealLastDayLocalDateTime = this.value.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();

        long days = ChronoUnit.DAYS.between(mealLastDayLocalDateTime, currentDate);

        return (int) days;
    }

    public Date getValue() {
        return value;
    }
}
