package com.mealsapp.mealsservice.domain;

public class InvalidMealPeriodicityException extends Exception {
    public InvalidMealPeriodicityException(String errorMessage) {
        super(errorMessage);
    }
}
