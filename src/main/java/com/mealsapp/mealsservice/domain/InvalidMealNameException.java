package com.mealsapp.mealsservice.domain;

public class InvalidMealNameException extends Exception {

    public InvalidMealNameException(String errorMessage) {
        super(errorMessage);
    }

}
