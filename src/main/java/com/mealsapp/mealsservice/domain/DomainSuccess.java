package com.mealsapp.mealsservice.domain;

public class DomainSuccess {

    private final String className = "DomainSuccess";
    private final String error = "Success";

    public String getClassName() {
        return className;
    }

    public String getError() {
        return error;
    }
}
