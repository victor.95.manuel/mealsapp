package com.mealsapp.mealsservice.domain;

import com.mealsapp.mealsservice.infrastructure.InvalidMealPeriodicity;
import com.mealsapp.mealsservice.infrastructure.InvalidMealType;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.stream.Collectors;

public class MealPeriodicity {

    private final int value;

    public MealPeriodicity(int value) {
        this.value = value;
    }

    public static <T> Result parseMealPeriodicity(int value) {
        if (value < 1) return Result.error(new InvalidMealPeriodicity(MealPeriodicity.class.getSimpleName()));

        return Result.valid(new MealPeriodicity(value));
    }

    public int getValue() {
        return value;
    }

    public boolean hasSpentPeriodicity(Meal meal) {
        LocalDateTime currentDate = LocalDateTime.now().truncatedTo(ChronoUnit.DAYS);
        LocalDateTime mealLastDay = meal.getMealLastDay().getValue()
                .toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();

        return ChronoUnit.DAYS.between(mealLastDay, currentDate) > meal.getMealPeriodicity().getValue();
    }
}
