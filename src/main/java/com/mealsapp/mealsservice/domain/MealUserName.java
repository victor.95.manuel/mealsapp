package com.mealsapp.mealsservice.domain;

import com.mealsapp.mealsservice.infrastructure.InvalidUserNameTextSize;

public class MealUserName {

    private static final int MIN_VALUE_LENGTH = 3;
    private static final int MAX_VALUE_LENGTH = 40;

    private final String value;

    public MealUserName(String value) {
        this.value = value.trim();
    }

    public static <T> Result parseMealUser(String value) {
        if (value == null || value.length() < MIN_VALUE_LENGTH || value.length() > MAX_VALUE_LENGTH)
            return Result.error(new InvalidUserNameTextSize(MealUserName.class.getSimpleName()));

        return Result.valid(new MealUserName(value));
    }

    public String getValue() {
        return this.value;
    }
}
