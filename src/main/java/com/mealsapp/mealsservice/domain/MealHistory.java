package com.mealsapp.mealsservice.domain;

public class MealHistory {

    private final String mealId;
    private final String mealName;
    private final MealLastDay mealLastDay;
    private final MealUserName username;

    public MealHistory(String mealId, String mealName, MealLastDay mealLastDay, MealUserName username) {
        this.mealId = mealId;
        this.mealName = mealName;
        this.mealLastDay = mealLastDay;
        this.username = username;
    }

    public String getMealId() {
        return mealId;
    }

    public String getMealName() {
        return mealName;
    }

    public MealLastDay getMealLastDay() {
        return mealLastDay;
    }

    public MealUserName getUsername() {
        return username;
    }
}
