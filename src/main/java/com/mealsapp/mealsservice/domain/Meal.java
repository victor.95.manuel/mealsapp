package com.mealsapp.mealsservice.domain;

import java.util.UUID;

public class Meal {

    private final String mealId;
    private final MealName mealName;
    private final MealType mealType;
    private final MealPeriodicity mealPeriodicity;
    private final MealLastDay mealLastDay;
    private final String discarded;
    private final MealUserName username;

    public Meal(MealName mealName, MealType mealType, MealPeriodicity mealPeriodicity, MealUserName username) {
        this.mealId = UUID.randomUUID().toString();
        this.mealName = mealName;
        this.mealType = mealType;
        this.mealPeriodicity = mealPeriodicity;
        this.mealLastDay = new MealLastDay(null);
        this.discarded = "0";
        this.username = username;
    }

    public Meal(MealName mealName, MealType mealType, MealPeriodicity mealPeriodicity, MealLastDay mealLastDay, MealUserName username) {
        this.mealId = UUID.randomUUID().toString();
        this.mealName = mealName;
        this.mealType = mealType;
        this.mealPeriodicity = mealPeriodicity;
        this.mealLastDay = mealLastDay;
        this.discarded = "0";
        this.username = username;
    }

    public Meal(String mealId, MealName mealName, MealType mealType, MealPeriodicity mealPeriodicity, MealLastDay mealLastDay, String discarded, MealUserName username) {
        this.mealId = mealId;
        this.mealName = mealName;
        this.mealType = mealType;
        this.mealPeriodicity = mealPeriodicity;
        this.mealLastDay = mealLastDay;
        this.discarded = discarded;
        this.username = username;
    }

    public String getMealId() {
        return mealId;
    }

    public MealName getMealName() {
        return mealName;
    }

    public MealType getMealType() {
        return mealType;
    }

    public MealPeriodicity getMealPeriodicity() {
        return mealPeriodicity;
    }

    public MealLastDay getMealLastDay() {
        return mealLastDay;
    }

    public String getDiscarded() {
        return discarded;
    }

    public MealUserName getUsername() {
        return username;
    }
}
