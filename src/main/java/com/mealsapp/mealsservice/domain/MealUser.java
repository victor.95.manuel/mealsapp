package com.mealsapp.mealsservice.domain;

import java.util.UUID;

public class MealUser {

    private final String userId;
    private final MealUserName userName;
    private final int activated;

    public MealUser(String userId, MealUserName userName) {
        this.userId = userId;
        this.userName = userName;
        this.activated = 0;
    }

    public MealUser(MealUserName userName) {
        this.userId = UUID.randomUUID().toString();
        this.userName = userName;
        this.activated = 0;
    }

    public String getUserId() {
        return userId;
    }

    public MealUserName getUserName() {
        return userName;
    }

    public int getActivated() {
        return activated;
    }
}
