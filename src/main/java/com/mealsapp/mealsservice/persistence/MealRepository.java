package com.mealsapp.mealsservice.persistence;

import com.mealsapp.mealsservice.domain.*;
import com.mealsapp.mealsservice.infrastructure.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Repository
public class MealRepository {

    private static final String LUNCH = "LUNCH";
    private static final String DINNER = "DINNER";
    private final MealDAO mealDAO;

    public MealRepository(MealDAO mealDAO) {
        this.mealDAO = mealDAO;
    }

    public String save(Meal meal) {
        return mealDAO.save(mapMealVO(meal)).getMealPrimaryKey().getMealid();
    }

    private MealVO mapMealVO(Meal meal) {
        return new MealVO(
                new MealPrimaryKey(meal.getMealId(), meal.getUsername().getValue()),
                meal.getMealName().getValue(),
                meal.getMealType().getValue().toString(),
                meal.getMealPeriodicity().getValue(),
                getMealLastDayFromRequest(meal),
                meal.getDiscarded()
        );
    }

    private Date getMealLastDayFromRequest(Meal meal) {
        if (meal.getMealLastDay() == null || meal.getMealLastDay().getValue() == null) return null;
        return meal.getMealLastDay().getValue();
    }

    public List<Meal> findAllMeals(String username) {
        List<Meal> collect = mealDAO.findAll()
                .stream()
                .filter(meal -> meal.getMealPrimaryKey().getUsername().equals(username))
                .map(this::mapToMealDomain)
                .collect(Collectors.toList());
        return collect;
    }

//    public List<Meal> findAllPageableMeals(Pageable pageable, String username) {
//        Page<MealVO> allMeals = mealDAO.findAll(pageable)
//                .stream()
//                .filter(meal -> meal.getMealPrimaryKey().getUsername().equals(username))
//                .
//
//        return allMeals
//                .stream()
//                .map(this::mapToMealDomain)
//                .collect(Collectors.toList());
//    }

    private Meal mapToMealDomain(MealVO mealVO) {
        return new Meal(
                mealVO.getMealPrimaryKey().getMealid(),
                (MealName) MealName.parseMealName(mealVO.getMealname()).getElement(),
                (MealType) MealType.parseMealType(mapToMealTypeValue(mealVO.getMealtype())).getElement(),
                (MealPeriodicity) MealPeriodicity.parseMealPeriodicity(mealVO.getMealperiodicity()).getElement(),
                new MealLastDay(mealVO.getMeallastday()),
                mealVO.getDiscarded(),
                (MealUserName) MealUserName.parseMealUser(mealVO.getMealPrimaryKey().getUsername()).getElement()
        );
    }

    private MealType.MealTypeValue mapToMealTypeValue(String mealtype) {
        switch (mealtype) {
            case LUNCH:
                return MealType.MealTypeValue.LUNCH;
            case DINNER:
                return MealType.MealTypeValue.DINNER;
            default:
                return null;
        }
    }

    public List<Meal> findMealsByType(MealTypeRequest mealTypeRequest) {
        List<MealVO> allMealsByType = mealDAO.findAll();
        return allMealsByType
                .stream()
                .filter(meal -> meal.getMealtype().equals(mealTypeRequest.getMealType().toUpperCase().trim()))
                .filter(meal -> meal.getMealPrimaryKey().getUsername().equals(mealTypeRequest.getUsername()))
                .map(this::mapToMealDomain)
                .collect(Collectors.toList());
    }

    public List<Meal> findNotEatenMeal(MealTypeRequest mealTypeRequest) {
        return mealDAO.findAll()
                .stream()
                .filter(meal -> meal.getMealtype().equals(mealTypeRequest.getMealType().toUpperCase().trim()))
                .filter(meal -> meal.getMealPrimaryKey().getUsername().equals(mealTypeRequest.getUsername()))
                .map(this::mapToMealDomain)
                .collect(Collectors.toList());
    }

    public Meal findMealToReset(MealResetRequest mealResetRequest) {
        MealVO mealFoundToReset = mealDAO.findAll()
                .stream()
                .filter(meal -> meal.getMealPrimaryKey().getMealid().equals(mealResetRequest.getMealid()))
                .filter(meal -> meal.getMealPrimaryKey().getUsername().equals(mealResetRequest.getUsername()))
                .collect(Collectors.toList())
                .get(0);

        if (mealFoundToReset == null) return null;

        return new Meal(
                mealFoundToReset.getMealPrimaryKey().getMealid(),
                (MealName) MealName.parseMealName(mealFoundToReset.getMealname()).getElement(),
                (MealType) MealType.parseMealType(mapToMealTypeValue(mealFoundToReset.getMealtype())).getElement(),
                (MealPeriodicity) MealPeriodicity.parseMealPeriodicity(mealFoundToReset.getMealperiodicity()).getElement(),
                new MealLastDay(mealFoundToReset.getMeallastday()),
                mealFoundToReset.getDiscarded(),
                (MealUserName) MealUserName.parseMealUser(mealFoundToReset.getMealPrimaryKey().getUsername()).getElement()
        );
    }

    public List<Meal> findMealsWithMaxRangeDate(MealTypeRequest mealTypeRequest) {
        return mealDAO.findMealsWithMaxRangeDate(mealTypeRequest.getMealType(), mealTypeRequest.getUsername())
                .stream()
                .filter(meal -> meal.getMealPrimaryKey().getUsername().equals(mealTypeRequest.getUsername()))
                .map(this::mapToMealDomain)
                .collect(Collectors.toList());
    }

    public void remove(Meal meal) {
        mealDAO.delete(mapMealVO(meal));
    }

    public List<Meal> findMealsByName(MealNameRequest mealNameRequest) {
        return mealDAO.findAll()
                .stream()
                .map(this::mapToMealDomain)
                .filter(meal -> formattedString(meal.getMealName().getValue()).contains(formattedString(mealNameRequest.getMealname())))
                .filter(meal -> meal.getUsername().getValue().equals(mealNameRequest.getUsername()))
                .collect(Collectors.toList());
    }

    private String formattedString(String valueToFormat) {
        return valueToFormat.toLowerCase().trim();
    }

//    public List<Meal> findMealByNamePageable(Pageable pageable, MealNameRequest mealNameRequest) {
//        return mealDAO.findAll()
//                .stream()
//                .map(this::mapToMealDomain)
//                .filter(meal -> formattedString(meal.getMealName().getValue()).contains(formattedString(mealNameRequest.getMealname())))
//                .filter(meal -> meal.getUsername().getValue().equals(mealNameRequest.getUsername()))
//                .collect(Collectors.toList());
//    }

    public List<Meal> findMealByNameAndTypePageable(MealNameRequest mealNameRequest, MealTypeRequest mealTypeRequest) {
        return mealDAO.findAll()
                .stream()
                .map(this::mapToMealDomain)
                .filter(meal -> formattedString(meal.getMealName().getValue()).contains(formattedString(mealNameRequest.getMealname())))
                .filter(meal -> meal.getMealType().getValue().toString().contains(mealTypeRequest.getMealType()))
                .filter(meal -> meal.getUsername().getValue().equals(mealTypeRequest.getUsername()))
                .collect(Collectors.toList());
    }

    public List<Meal> findMealByNameAndTypeAndLastDayPageable(MealNameRequest mealNameRequest,
                                                              MealTypeRequest mealTypeRequest,
                                                              MealLastDayRequest mealLastDayRequest,
                                                              boolean emptyCheckBox) {
        return mealDAO.findAll()
                .stream()
                .map(this::mapToMealDomain)
                .filter(meal -> filterByName(meal, mealNameRequest))
                .filter(meal -> filterByType(meal, mealTypeRequest))
                .filter(meal -> filterByLastDay(meal, mealLastDayRequest, emptyCheckBox))
                .filter(meal -> filterByUsername(meal, mealTypeRequest))
                .collect(Collectors.toList());
    }

    private boolean filterByName(Meal meal, MealNameRequest mealNameRequest) {
        if (mealNameRequest == null || mealNameRequest.getMealname().isEmpty()) return true;
        return meal.getMealName().getValue().toUpperCase().contains(mealNameRequest.getMealname().toUpperCase());
    }

    private boolean filterByType(Meal meal, MealTypeRequest mealTypeRequest) {
        if (mealTypeRequest == null || mealTypeRequest.getMealType().isEmpty()) return true;
        return meal.getMealType().getValue().toString().contains(mealTypeRequest.getMealType());
    }

    private boolean filterByLastDay(Meal meal, MealLastDayRequest mealLastDayRequest, boolean emptyCheckBox) {

        if (!emptyCheckBox && mealLastDayRequest.getMeallastday().isEmpty()) return true;

        try {
            if (emptyCheckBox)
                return meal.getMealLastDay() == null || meal.getMealLastDay().getValue() == null;

            if (meal.getMealLastDay() == null || meal.getMealLastDay().getValue() == null) return false;

            return meal.getMealLastDay().getValue().toInstant()
                    .equals(new SimpleDateFormat("yyyy-MM-dd").parse(mealLastDayRequest.getMeallastday()).toInstant());

        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean filterByUsername(Meal meal, MealTypeRequest mealTypeRequest) {
        if (mealTypeRequest == null || mealTypeRequest.getMealType().isEmpty()) return true;
        return meal.getUsername().getValue().equals(mealTypeRequest.getUsername());
    }
}
