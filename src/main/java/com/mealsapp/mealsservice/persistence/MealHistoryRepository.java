package com.mealsapp.mealsservice.persistence;

import com.mealsapp.mealsservice.domain.*;
import com.mealsapp.mealsservice.infrastructure.MealLastDayRequest;
import com.mealsapp.mealsservice.infrastructure.MealNameRequest;
import com.mealsapp.mealsservice.infrastructure.MealRequest;
import org.springframework.stereotype.Repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Repository
public class MealHistoryRepository {

    private static final String LUNCH = "LUNCH";
    private static final String DINNER = "DINNER";

    private final MealHistoryDAO mealHistoryDAO;
    private final MealDAO mealDAO;
    private final MealRepository mealRepository;

    public MealHistoryRepository(MealHistoryDAO mealHistoryDAO, MealDAO mealDAO, MealRepository mealRepository) {
        this.mealHistoryDAO = mealHistoryDAO;
        this.mealDAO = mealDAO;
        this.mealRepository = mealRepository;
    }

    public String save(MealHistory meal) {
        return mealHistoryDAO.save(mapMealHistoryVO(meal)).getMealhistoryprimarykey().getMealid();
    }

    private MealHistoryVO mapMealHistoryVO(MealHistory meal) {
        return new MealHistoryVO(
                new MealHistoryPrimaryKey(
                        meal.getMealId(),
                        meal.getMealLastDay().getValue(),
                        meal.getUsername().getValue()
                ),
                meal.getMealName()
        );
    }

    public List<Meal> findAllMealsByDate(MealLastDayRequest mealLastDayRequest) {
        List<MealHistoryVO> allHistoryMeals = mealHistoryDAO.findAll()
                .stream()
                .filter(meal -> filterByUsernameHistory(meal, mealLastDayRequest))
                .filter(meal -> isTheSameMealLastDay(meal, mealLastDayRequest))
                .collect(Collectors.toList());

        return getMealsFromHistory(allHistoryMeals)
                .stream()
                .filter(meal -> filterByUsername(meal, mealLastDayRequest))
                .map(this::mapMeal)
                .collect(Collectors.toList());
    }

    private boolean filterByUsername(MealVO meal, MealLastDayRequest mealLastDayRequest) {
        return meal.getMealPrimaryKey().getUsername().equals(mealLastDayRequest.getUsername());
    }

    private boolean filterByUsernameHistory(MealHistoryVO meal, MealLastDayRequest mealLastDayRequest) {
        return meal.getMealhistoryprimarykey().getUsername().equals(mealLastDayRequest.getUsername());
    }

    private boolean isTheSameMealId(MealVO mealVO, MealHistoryVO mealHistoryVO) {
        String mealIdVO = mealVO.getMealPrimaryKey().getMealid();
        String mealHistoryIdVO = mealHistoryVO.getMealhistoryprimarykey().getMealid();

        return mealIdVO.equals(mealHistoryIdVO);
    }

    private Meal mapMeal(MealVO mealVO) {
        return new Meal(
                mealVO.getMealPrimaryKey().getMealid(),
                (MealName) MealName.parseMealName(mealVO.getMealname()).getElement(),
                (MealType) MealType.parseMealType(mapToMealTypeValue(mealVO.getMealtype())).getElement(),
                (MealPeriodicity) MealPeriodicity.parseMealPeriodicity(mealVO.getMealperiodicity()).getElement(),
                new MealLastDay(mealVO.getMeallastday()),
                mealVO.getDiscarded(),
                (MealUserName) MealUserName.parseMealUser(mealVO.getMealPrimaryKey().getUsername()).getElement()
        );
    }

    private MealType.MealTypeValue mapToMealTypeValue(String mealtype) {
        switch (mealtype) {
            case LUNCH:
                return MealType.MealTypeValue.LUNCH;
            case DINNER:
                return MealType.MealTypeValue.DINNER;
            default:
                return null;
        }
    }

    private boolean isTheSameMealLastDay(MealHistoryVO meal, MealLastDayRequest mealLastDayRequest) {
        try {

            Date requestDate = new SimpleDateFormat("yyyy-MM-dd").parse(mealLastDayRequest.getMeallastday());
            return requestDate.equals(meal.getMealhistoryprimarykey().getMeallastday());

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return false;
    }

    public List<MealHistoryVO> findAllMealHistory() {
        return mealHistoryDAO.findAll();
    }

    public List<MealHistory> findAllPageableMealHistory(String username) {
        return mealHistoryDAO.findAll()
                .stream()
                .filter(meal -> meal.getMealhistoryprimarykey().getUsername().equals(username))
                .map(this::mapToMealHistoryDomain)
                .collect(Collectors.toList());
    }

    private MealHistory mapToMealHistoryDomain(MealHistoryVO mealHistoryVO) {
        return new MealHistory(
                mealHistoryVO.getMealhistoryprimarykey().getMealid(),
                mealHistoryVO.getMealname(),
                new MealLastDay(mealHistoryVO.getMealhistoryprimarykey().getMeallastday()),
                new MealUserName(mealHistoryVO.getMealhistoryprimarykey().getUsername())
        );
    }

    public List<Meal> findDatesOfMeal(MealRequest mealRequest) {
        List<MealHistoryVO> allHistoryMeals = mealHistoryDAO.findAll()
                .stream()
                .filter(meal -> isTheSameMeal(meal, mealRequest))
                .collect(Collectors.toList());

        return getMealsFromHistory(allHistoryMeals)
                .stream()
                .map(this::mapMeal)
                .collect(Collectors.toList());
    }

    private List<MealVO> getMealsFromHistory(List<MealHistoryVO> allHistoryMeals) {
        List<MealVO> allDatesOfMeal = new ArrayList<>();
        for (MealHistoryVO mealHistoryVO : allHistoryMeals) {
            MealVO mealVOToAdd = mealDAO.findAll()
                    .stream()
                    .filter(meal -> isTheSameMealId(meal, mealHistoryVO))
                    .collect(Collectors.toList())
                    .get(0);

            allDatesOfMeal.add(new MealVO(
                    mealVOToAdd.getMealPrimaryKey(),
                    mealVOToAdd.getMealname(),
                    mealVOToAdd.getMealtype(),
                    mealVOToAdd.getMealperiodicity(),
                    mealHistoryVO.getMealhistoryprimarykey().getMeallastday(),
                    mealVOToAdd.getDiscarded()
            ));
        }

        return allDatesOfMeal;
    }

    private boolean isTheSameMeal(MealHistoryVO mealHistoryVO, MealRequest mealRequest) {
        return mealHistoryVO.getMealhistoryprimarykey().getMealid().equals(
                mealDAO.findAll()
                        .stream()
                        .filter(meal -> meal.getMealname().equals(mealRequest.getMealName()))
                        .filter(meal -> meal.getMealtype().equals(mealRequest.getMealType()))
                        .collect(Collectors.toList())
                        .get(0)
                        .getMealPrimaryKey().getMealid()
        );
    }

    public void remove(MealHistory mealHistory) {
        mealHistoryDAO.delete(mapMealHistoryVO(mealHistory));
    }

    public List<MealHistory> findHistoryMealByNameAndLastDayPageable(MealNameRequest mealNameRequest, MealLastDayRequest mealLastDayRequest) {
        return mealHistoryDAO.findAll()
                .stream()
                .filter(meal -> filterByUsername(mealNameRequest, meal))
                .filter(meal -> filterByNameHistory(meal, mealNameRequest))
                .filter(meal -> filterByLastDayHistory(meal, mealLastDayRequest))
                .map(this::mapToMealHistoryDomain)
                .collect(Collectors.toList());
    }

    private boolean filterByUsername(MealNameRequest mealNameRequest, MealHistoryVO meal) {
        return meal.getMealhistoryprimarykey().getUsername().equals(mealNameRequest.getUsername());
    }

    private boolean filterByNameHistory(MealHistoryVO meal, MealNameRequest mealNameRequest) {
        if (mealNameRequest == null || mealNameRequest.getMealname().isEmpty()) return true;
        return meal.getMealname().contains(mealNameRequest.getMealname());
    }

    private boolean filterByLastDayHistory(MealHistoryVO mealHistoryVO, MealLastDayRequest mealLastDayRequest) {

        if (mealLastDayRequest.getMeallastday().isEmpty()) return true;

        try {
            if (mealHistoryVO.getMealhistoryprimarykey().getMeallastday() == null ||
                    mealHistoryVO.getMealhistoryprimarykey().getMeallastday().toString().isEmpty())
                return false;

            return mealHistoryVO.getMealhistoryprimarykey().getMeallastday().toInstant()
                    .equals(new SimpleDateFormat("yyyy-MM-dd").parse(mealLastDayRequest.getMeallastday()).toInstant());

        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }
    }

}
