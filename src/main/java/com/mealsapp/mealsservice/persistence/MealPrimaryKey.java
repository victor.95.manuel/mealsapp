package com.mealsapp.mealsservice.persistence;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class MealPrimaryKey implements Serializable {

    public MealPrimaryKey() {
    }

    public String mealid;
    public String username;

    public MealPrimaryKey(String mealid, String username) {
        this.mealid = mealid;
        this.username = username;
    }

    public String getMealid() {
        return mealid;
    }

    public String getUsername() {
        return username;
    }
}
