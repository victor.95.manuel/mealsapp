package com.mealsapp.mealsservice.persistence;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Date;

@Embeddable
public class MealHistoryPrimaryKey implements Serializable {

    public MealHistoryPrimaryKey() {
    }

    public String mealid;
    public Date meallastday;
    public String username;

    public MealHistoryPrimaryKey(String mealid, Date meallastday, String username) {
        this.mealid = mealid;
        this.meallastday = meallastday;
        this.username = username;
    }

    public String getMealid() {
        return mealid;
    }

    public Date getMeallastday() {
        return meallastday;
    }

    public String getUsername() {
        return username;
    }
}
