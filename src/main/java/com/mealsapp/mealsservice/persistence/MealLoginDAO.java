package com.mealsapp.mealsservice.persistence;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MealLoginDAO extends CrudRepository<MealLoginVO, String> {

    List<MealLoginVO> findAll();

}
