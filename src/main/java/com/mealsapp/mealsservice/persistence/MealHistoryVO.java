package com.mealsapp.mealsservice.persistence;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "meal_history")
public class MealHistoryVO {

    @EmbeddedId
    public MealHistoryPrimaryKey mealhistoryprimarykey;
    public String mealname;

    public MealHistoryVO() {
    }

    public MealHistoryVO(MealHistoryPrimaryKey mealhistoryprimarykey, String mealname) {
        this.mealhistoryprimarykey = mealhistoryprimarykey;
        this.mealname = mealname;
    }

    public MealHistoryPrimaryKey getMealhistoryprimarykey() {
        return mealhistoryprimarykey;
    }

    public String getMealname() {
        return mealname;
    }
}
