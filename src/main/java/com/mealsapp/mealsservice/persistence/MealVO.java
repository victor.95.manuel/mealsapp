package com.mealsapp.mealsservice.persistence;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "meal")
public class MealVO {

    @EmbeddedId
    public MealPrimaryKey mealPrimaryKey;
    public String mealname;
    public String mealtype;
    public int mealperiodicity;
    public Date meallastday;
    public String discarded;

    public MealVO() {
    }

    public MealVO(MealPrimaryKey mealPrimaryKey, String mealname, String mealtype, int mealperiodicity, Date meallastday, String discarded) {
        this.mealPrimaryKey = mealPrimaryKey;
        this.mealname = mealname;
        this.mealtype = mealtype;
        this.mealperiodicity = mealperiodicity;
        this.meallastday = meallastday;
        this.discarded = discarded;
    }

    public MealPrimaryKey getMealPrimaryKey() {
        return mealPrimaryKey;
    }

    public String getMealname() {
        return mealname;
    }

    public String getMealtype() {
        return mealtype;
    }

    public int getMealperiodicity() {
        return mealperiodicity;
    }

    public Date getMeallastday() {
        return meallastday;
    }

    public String getDiscarded() {
        return discarded;
    }
}
