package com.mealsapp.mealsservice.persistence;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "meal_user")
public class MealLoginVO {

    @Id
    public String userid;
    public String username;
    public int activated;

    public MealLoginVO() {
    }

    public MealLoginVO(String userid, String username, int activated) {
        this.userid = userid;
        this.username = username;
        this.activated = activated;
    }

    public String getUserid() {
        return userid;
    }

    public String getUsername() {
        return username;
    }

    public int getActivated() {
        return activated;
    }
}
