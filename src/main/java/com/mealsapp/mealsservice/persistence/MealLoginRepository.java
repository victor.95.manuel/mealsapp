package com.mealsapp.mealsservice.persistence;

import com.mealsapp.mealsservice.domain.MealUser;
import com.mealsapp.mealsservice.domain.MealUserName;
import com.mealsapp.mealsservice.infrastructure.MealLoginRequest;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class MealLoginRepository {

    private final int ACTIVATED = 1;
    private final MealLoginDAO mealLoginDAO;

    public MealLoginRepository(MealLoginDAO mealLoginDAO) {
        this.mealLoginDAO = mealLoginDAO;
    }

    public MealUser findUser(MealLoginRequest mealLoginRequest) {
        List<MealLoginVO> allUsers = mealLoginDAO.findAll();

        List<MealLoginVO> usersFound = mealLoginDAO.findAll()
                .stream()
                .filter(user -> filterByUsername(user, mealLoginRequest))
                .filter(this::filterByUserActivated)
                .collect(Collectors.toList());

        if (usersFound.isEmpty()) return null;
        return new MealUser(new MealUserName(usersFound.get(0).getUsername()));
    }

    private boolean filterByUserActivated(MealLoginVO user) {
        return user.getActivated() == ACTIVATED;
    }

    private boolean filterByUsername(MealLoginVO user, MealLoginRequest mealLoginRequest) {
        return user.getUsername().equals(mealLoginRequest.getUsername());
    }
}
