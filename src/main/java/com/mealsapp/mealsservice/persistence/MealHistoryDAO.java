package com.mealsapp.mealsservice.persistence;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MealHistoryDAO extends CrudRepository<MealHistoryVO, String> {

    List<MealHistoryVO> findAll();

}
