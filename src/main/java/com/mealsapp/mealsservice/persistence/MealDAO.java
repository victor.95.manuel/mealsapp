package com.mealsapp.mealsservice.persistence;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MealDAO extends CrudRepository<MealVO, String> {

    List<MealVO> findAll();
    Page<MealVO> findAll(Pageable pageable);

    @Query(value =
            "SELECT M.* FROM meal M " +
                    "WHERE M.MEALLASTDAY IS NOT NULL " +
                    "AND M.MEALTYPE = ?1 " +
                    "AND M.DISCARDED = '0' " +
                    "AND M.USERNAME = ?2 " +
                    "AND DATE(SYSDATE()) - M.MEALLASTDAY = " +
                    "(SELECT MAX(DATE(SYSDATE()) - T.MEALLASTDAY) " +
                    "FROM meal T " +
                    "WHERE T.MEALTYPE = ?1 " +
                    "AND T.DISCARDED = '0' " +
                    "AND T.MEALLASTDAY IS NOT NULL " +
                    "AND T.USERNAME = ?2)",
            nativeQuery = true)
    List<MealVO> findMealsWithMaxRangeDate(String mealType, String username);
}
