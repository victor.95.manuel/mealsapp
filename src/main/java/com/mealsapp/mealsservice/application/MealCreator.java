package com.mealsapp.mealsservice.application;

import com.mealsapp.mealsservice.domain.Meal;
import com.mealsapp.mealsservice.persistence.MealRepository;
import org.springframework.stereotype.Service;

@Service
public class MealCreator {

    private final MealRepository mealRepository;

    public MealCreator(MealRepository mealRepository) {
        this.mealRepository = mealRepository;
    }

    public String create(Meal mealToSave) {
        return mealRepository.save(mealToSave);
    }
}
