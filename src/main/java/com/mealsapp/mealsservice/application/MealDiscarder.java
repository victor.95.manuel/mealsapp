package com.mealsapp.mealsservice.application;

import com.mealsapp.mealsservice.domain.Meal;
import com.mealsapp.mealsservice.domain.MealHistory;
import com.mealsapp.mealsservice.domain.MealLastDay;
import com.mealsapp.mealsservice.domain.MealUserName;
import com.mealsapp.mealsservice.infrastructure.MealRequest;
import com.mealsapp.mealsservice.infrastructure.MealUserRequest;
import com.mealsapp.mealsservice.persistence.MealHistoryRepository;
import com.mealsapp.mealsservice.persistence.MealHistoryVO;
import com.mealsapp.mealsservice.persistence.MealRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MealDiscarder {

    private final MealRepository mealRepository;
    private final MealHistoryRepository mealHistoryRepository;

    private final String DISCARDED = "1";
    private final String NOT_DISCARDED = "0";

    public MealDiscarder(MealRepository mealRepository, MealHistoryRepository mealHistoryRepository) {
        this.mealRepository = mealRepository;
        this.mealHistoryRepository = mealHistoryRepository;
    }

    public void discardMeal(MealRequest mealToDiscardRequest) {
        Meal mealToDiscard = mealRepository.findAllMeals(mealToDiscardRequest.getUsername())
                .stream()
                .filter(meal -> getMealToDiscard(meal, mealToDiscardRequest))
                .filter(meal -> meal.getUsername().getValue().equals(mealToDiscardRequest.getUsername()))
                .collect(Collectors.toList())
                .get(0);

        mealRepository.save(new Meal(
                mealToDiscard.getMealId(),
                mealToDiscard.getMealName(),
                mealToDiscard.getMealType(),
                mealToDiscard.getMealPeriodicity(),
                mealToDiscard.getMealLastDay(),
                DISCARDED,
                mealToDiscard.getUsername()
        ));

        MealHistory mealHistoryToRemove = mealHistoryRepository.findAllMealHistory()
                .stream()
                .filter(meal -> isTheMealHistoryToRemove(meal, mealToDiscard))
                .filter(meal -> meal.getMealhistoryprimarykey().getUsername().equals(mealToDiscardRequest.getUsername()))
                .map(meal -> mapToMealHistory(meal, mealToDiscardRequest))
                .collect(Collectors.toList())
                .get(0);

        Optional<MealHistory> lastMealFromMealHistory = mealHistoryRepository.findAllMealHistory()
                .stream()
                .filter(meal -> getPreviousMealHistory(meal, mealHistoryToRemove))
                .filter(meal -> meal.getMealhistoryprimarykey().getUsername().equals(mealToDiscardRequest.getUsername()))
                .map(meal -> mapToMealHistory(meal, mealToDiscardRequest))
                .max(this::maxMealLastDay);

        MealLastDay mealLastDayToUpdate = new MealLastDay(null);

        if (lastMealFromMealHistory.isPresent()) mealLastDayToUpdate = lastMealFromMealHistory.get().getMealLastDay();

        mealHistoryRepository.remove(mealHistoryToRemove);

        mealRepository.save(new Meal(
                mealToDiscard.getMealId(),
                mealToDiscard.getMealName(),
                mealToDiscard.getMealType(),
                mealToDiscard.getMealPeriodicity(),
                mealLastDayToUpdate,
                "1",
                mealToDiscard.getUsername()
        ));
    }

    private MealHistory mapToMealHistory(MealHistoryVO mealHistoryVO, MealRequest mealToDiscardRequest) {
        return new MealHistory(
                mealHistoryVO.getMealhistoryprimarykey().getMealid(),
                mealToDiscardRequest.getMealName(),
                new MealLastDay(mealHistoryVO.getMealhistoryprimarykey().getMeallastday()),
                new MealUserName(mealHistoryVO.getMealhistoryprimarykey().getUsername())
        );
    }

    private int maxMealLastDay(MealHistory mealHistory, MealHistory mealHistory1) {
        return mealHistory.getMealLastDay().getValue().compareTo(mealHistory1.getMealLastDay().getValue());
    }

    private boolean isTheMealHistoryToRemove(MealHistoryVO meal, Meal mealToDiscard) {
        return meal.getMealhistoryprimarykey().getMealid().equals(mealToDiscard.getMealId()) &&
                meal.getMealhistoryprimarykey().getMeallastday().equals(mealToDiscard.getMealLastDay().getValue());
    }


    private boolean getPreviousMealHistory(MealHistoryVO meal, MealHistory mealHistoryToDiscard) {
        return meal.getMealhistoryprimarykey().getMealid().equals(mealHistoryToDiscard.getMealId()) &&
                meal.getMealhistoryprimarykey().getMeallastday().before(mealHistoryToDiscard.getMealLastDay().getValue());
    }

    private boolean getMealToDiscard(Meal meal, MealRequest mealToDiscardRequest) {
        return meal.getMealName().getValue().equals(mealToDiscardRequest.getMealName()) &&
                meal.getMealType().getValue().toString().equals(mealToDiscardRequest.getMealType().toUpperCase());
    }

    public void cleanDiscardedMeal(MealUserRequest mealUserRequest) {
        mealRepository.findAllMeals(mealUserRequest.getUsername())
                .stream()
                .filter(meal -> meal.getDiscarded().equals(DISCARDED))
                .filter(meal -> meal.getUsername().getValue().equals(mealUserRequest.getUsername()))
                .forEach(meal -> mealRepository.save(new Meal(
                        meal.getMealId(),
                        meal.getMealName(),
                        meal.getMealType(),
                        meal.getMealPeriodicity(),
                        meal.getMealLastDay(),
                        NOT_DISCARDED,
                        meal.getUsername()
                )));
    }
}
