package com.mealsapp.mealsservice.application;

import com.mealsapp.mealsservice.domain.*;
import com.mealsapp.mealsservice.infrastructure.*;
import com.mealsapp.mealsservice.persistence.MealHistoryRepository;
import com.mealsapp.mealsservice.persistence.MealRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Service
public class MealFinder {

    public static final String LUNCH = "LUNCH";
    public static final String DINNER = "DINNER";
    public static final String NOT_DISCARDED = "0";
    public static final String DISCARDED = "1";

    private final MealRepository mealRepository;
    private final MealHistoryRepository mealHistoryRepository;

    public MealFinder(MealRepository mealRepository, MealHistoryRepository mealHistoryRepository) {
        this.mealRepository = mealRepository;
        this.mealHistoryRepository = mealHistoryRepository;
    }

    public List<MealResponse> findAllMeals(String username) {
        List<MealResponse> collect = mealRepository.findAllMeals(username)
                .stream()
                .map(meal -> new MealResponse(meal.getMealId(),
                        meal.getMealName().getValue(),
                        meal.getMealType().getValue().toString(),
                        meal.getMealPeriodicity().getValue(),
                        meal.getMealLastDay().getValue(),
                        meal.getDiscarded(),
                        meal.getUsername().getValue()))
                .collect(Collectors.toList());
        return collect;
    }

//    public List<MealResponse> findAllPageableMeals(Pageable pageable, String username) {
//        return mealRepository.findAllPageableMeals(pageable, username)
//                .stream()
//                .filter(meal -> meal.getUsername().getValue().equals(username))
//                .map(meal -> new MealResponse(meal.getMealId(),
//                        meal.getMealName().getValue(),
//                        meal.getMealType().getValue().toString(),
//                        meal.getMealPeriodicity().getValue(),
//                        meal.getMealLastDay().getValue(),
//                        meal.getDiscarded(),
//                        meal.getUsername().getValue()))
//                .collect(Collectors.toList());
//    }

    public List<MealResponse> findMealsByType(MealTypeRequest mealTypeRequest) {
        return mealRepository.findMealsByType(mealTypeRequest)
                .stream()
                .filter(meal -> meal.getUsername().getValue().equals(mealTypeRequest.getUsername()))
                .map(meal -> new MealResponse(meal.getMealId(),
                        meal.getMealName().getValue(),
                        meal.getMealType().getValue().toString(),
                        meal.getMealPeriodicity().getValue(),
                        meal.getMealLastDay().getValue(),
                        meal.getDiscarded(),
                        meal.getUsername().getValue()))
                .collect(Collectors.toList());
    }

    public List<MealResponse> findMealsByDate(MealLastDayRequest mealLastDayRequest) {
        return mealHistoryRepository.findAllMealsByDate(mealLastDayRequest)
                .stream()
                .filter(meal -> meal.getUsername().getValue().equals(mealLastDayRequest.getUsername()))
                .map(meal -> new MealResponse(meal.getMealId(),
                        meal.getMealName().getValue(),
                        meal.getMealType().getValue().toString(),
                        meal.getMealPeriodicity().getValue(),
                        meal.getMealLastDay().getValue(),
                        meal.getDiscarded(),
                        meal.getUsername().getValue()))
                .collect(Collectors.toList());
    }

    public MealResponse findNotEatenMeal(MealTypeRequest mealTypeRequest) {

        checkDiscardedMeals(mealTypeRequest);

        List<MealResponse> allMealsWithoutLastDay = mealRepository.findNotEatenMeal(mealTypeRequest)
                .stream()
                .map(meal -> new MealResponse(meal.getMealId(),
                        meal.getMealName().getValue(),
                        meal.getMealType().getValue().toString(),
                        meal.getMealPeriodicity().getValue(),
                        meal.getMealLastDay().getValue(),
                        meal.getDiscarded(),
                        meal.getUsername().getValue()))
                .filter(this::isMealWithoutLastDay)
                .filter(meal -> meal.getDiscarded().equals(NOT_DISCARDED))
                .filter(meal -> meal.getUsername().equals(mealTypeRequest.getUsername()))
                .collect(Collectors.toList());

        if (allMealsWithoutLastDay.isEmpty()) return findMealToEatWithMaxDate(mealTypeRequest);

        MealResponse mealResponseToUpdate = allMealsWithoutLastDay.get(0);

        updateMealLastDay(mealResponseToUpdate);
        updateMealHistory(mealResponseToUpdate);

        return allMealsWithoutLastDay.get(0);
    }

    private void checkDiscardedMeals(MealTypeRequest mealTypeRequest) {
        List<Meal> allMeals = mealRepository.findAllMeals(mealTypeRequest.getUsername())
                .stream()
                .filter(meal -> meal.getMealType().getValue().toString().equals(mealTypeRequest.getMealType()))
                .collect(Collectors.toList());

        List<Meal> discardedMeals = mealRepository.findAllMeals(mealTypeRequest.getUsername())
                .stream()
                .filter(meal -> meal.getMealType().getValue().toString().equals(mealTypeRequest.getMealType()))
                .filter(meal -> meal.getDiscarded().equals(DISCARDED))
                .collect(Collectors.toList());

        if (!allMeals.isEmpty() && allMeals.size() == discardedMeals.size())
            updateDiscardedMeals(discardedMeals.get(0).getUsername().getValue());
    }

    private void updateMealHistory(MealResponse mealResponseToUpdate) {
        mealHistoryRepository.save(mapToHistoryMeal(mealResponseToUpdate));
    }

    private MealHistory mapToHistoryMeal(MealResponse mealResponseToUpdate) {
        Date currentDate = Date
                .from(
                        LocalDateTime
                                .now()
                                .truncatedTo(ChronoUnit.DAYS)
                                .atZone(ZoneId.systemDefault())
                                .toInstant()
                );

        return new MealHistory(
                mealResponseToUpdate.getMealid(),
                mealResponseToUpdate.getMealName(),
                new MealLastDay(currentDate),
                new MealUserName(mealResponseToUpdate.getUsername())
        );
    }

    private void updateMealLastDay(MealResponse mealResponseToUpdate) {
        Date currentDate = Date
                .from(
                        LocalDateTime
                                .now()
                                .truncatedTo(ChronoUnit.DAYS)
                                .atZone(ZoneId.systemDefault())
                                .toInstant()
                );

        Meal mealToUpdate = new Meal(
                mealResponseToUpdate.getMealid(),
                (MealName) MealName.parseMealName(mealResponseToUpdate.getMealName()).getElement(),
                (MealType) MealType.parseMealType(mapToMealTypeValue(mealResponseToUpdate.getMealType())).getElement(),
                (MealPeriodicity) MealPeriodicity.parseMealPeriodicity(mealResponseToUpdate.getMealPeriodicity()).getElement(),
                new MealLastDay(currentDate),
                mealResponseToUpdate.getDiscarded(),
                (MealUserName) MealUserName.parseMealUser(mealResponseToUpdate.getUsername()).getElement()
        );

        mealRepository.save(mealToUpdate);
    }

    private MealType.MealTypeValue mapToMealTypeValue(String mealTypeValue) {
        switch (mealTypeValue) {
            case LUNCH:
                return MealType.MealTypeValue.LUNCH;
            case DINNER:
                return MealType.MealTypeValue.DINNER;
            default:
                return null;
        }
    }

    private MealResponse findMealToEatWithMaxDate(MealTypeRequest mealTypeRequest) {
        List<MealResponse> mealsWithMaxRangeDate = mealRepository.findMealsWithMaxRangeDate(mealTypeRequest)
                .stream()
                .filter(meal -> meal.getUsername().getValue().equals(mealTypeRequest.getUsername()))
                .map(meal -> new MealResponse(meal.getMealId(),
                        meal.getMealName().getValue(),
                        meal.getMealType().getValue().toString(),
                        meal.getMealPeriodicity().getValue(),
                        meal.getMealLastDay().getValue(),
                        meal.getDiscarded(),
                        meal.getUsername().getValue()))
                .collect(Collectors.toList());

        if (mealsWithMaxRangeDate.isEmpty()) return null;
        return findRandomMealToEat(mealsWithMaxRangeDate, mealTypeRequest.getUsername());
    }

    private MealResponse findRandomMealToEat(List<MealResponse> mealsWithMaxRangeDate, String username) {
        List<MealResponse> mealResponseToUpdateList = mealsWithMaxRangeDate
                .stream()
                .filter(meal -> meal.getUsername().equals(username))
                .filter(meal -> meal.getDiscarded().equals(NOT_DISCARDED))
                .collect(Collectors.toList());

        MealResponse mealResponseToUpdate = mealResponseToUpdateList
                .get(new Random().nextInt(mealResponseToUpdateList.size()));

        updateMealLastDay(mealResponseToUpdate);
        updateMealHistory(mealResponseToUpdate);

        return mealResponseToUpdate;
    }

    private void updateDiscardedMeals(String username) {
        mealRepository.findAllMeals(username)
                .stream()
                .filter(meal -> meal.getDiscarded().equals(DISCARDED))
                .forEach(meal -> mealRepository.save(new Meal(
                        meal.getMealId(),
                        meal.getMealName(),
                        meal.getMealType(),
                        meal.getMealPeriodicity(),
                        meal.getMealLastDay(),
                        NOT_DISCARDED,
                        meal.getUsername()
                )));
    }

    private boolean isMealWithoutLastDay(MealResponse mealResponse) {
        return mealResponse.getMealLastDay() == null;
    }

    public List<MealResponse> findDatesOfMeal(MealRequest mealRequest) {
        return mealHistoryRepository.findDatesOfMeal(mealRequest)
                .stream()
                .filter(meal -> meal.getUsername().getValue().equals(mealRequest.getUsername()))
                .map(meal -> new MealResponse(meal.getMealId(),
                        meal.getMealName().getValue(),
                        meal.getMealType().getValue().toString(),
                        meal.getMealPeriodicity().getValue(),
                        meal.getMealLastDay().getValue(),
                        meal.getDiscarded(),
                        meal.getUsername().getValue()))
                .collect(Collectors.toList());
    }

    public List<MealResponse> findMealByName(MealRequest mealRequest) {
        return mealRepository.findMealsByName(new MealNameRequest(mealRequest.getMealName(), mealRequest.getUsername()))
                .stream()
                .filter(meal -> meal.getUsername().getValue().equals(mealRequest.getUsername()))
                .map(meal -> new MealResponse(meal.getMealId(),
                        meal.getMealName().getValue(),
                        meal.getMealType().getValue().toString(),
                        meal.getMealPeriodicity().getValue(),
                        meal.getMealLastDay().getValue(),
                        meal.getDiscarded(),
                        meal.getUsername().getValue()))
                .collect(Collectors.toList());
    }

    public List<MealResponse> findMealByNamePageable(Pageable pageable, MealRequest mealRequest) {
        return mealRepository.findMealByNamePageable(pageable, new MealNameRequest(mealRequest.getMealName(), mealRequest.getUsername()))
                .stream()
                .filter(meal -> meal.getUsername().getValue().equals(mealRequest.getUsername()))
                .map(meal -> new MealResponse(meal.getMealId(),
                        meal.getMealName().getValue(),
                        meal.getMealType().getValue().toString(),
                        meal.getMealPeriodicity().getValue(),
                        meal.getMealLastDay().getValue(),
                        meal.getDiscarded(),
                        meal.getUsername().getValue()))
                .collect(Collectors.toList());
    }

    public List<MealResponse> findMealByNameAndTypePageable(MealRequest mealRequest, MealTypeRequest mealTypeRequest) {
        return mealRepository.findMealByNameAndTypePageable(new MealNameRequest(mealRequest.getMealName(), mealRequest.getUsername()),
                        new MealTypeRequest(mealTypeRequest.getMealType()))
                .stream()
                .filter(meal -> meal.getUsername().getValue().equals(mealRequest.getUsername()))
                .map(meal -> new MealResponse(meal.getMealId(),
                        meal.getMealName().getValue(),
                        meal.getMealType().getValue().toString(),
                        meal.getMealPeriodicity().getValue(),
                        meal.getMealLastDay().getValue(),
                        meal.getDiscarded(),
                        meal.getUsername().getValue()))
                .collect(Collectors.toList());
    }

    public List<MealResponse> findMealByNameAndTypeAndLastDayPageable(MealRequest mealRequest,
                                                                      MealTypeRequest mealTypeRequest,
                                                                      MealLastDayRequest mealLastDayRequest,
                                                                      boolean emptyCheckBox) {
        return mealRepository.findMealByNameAndTypeAndLastDayPageable(
                        new MealNameRequest(mealRequest.getMealName(), mealRequest.getUsername()),
                        mealTypeRequest,
                        mealLastDayRequest,
                        emptyCheckBox)
                .stream()
                .filter(meal -> meal.getUsername().getValue().equals(mealRequest.getUsername()))
                .map(meal -> new MealResponse(meal.getMealId(),
                        meal.getMealName().getValue(),
                        meal.getMealType().getValue().toString(),
                        meal.getMealPeriodicity().getValue(),
                        meal.getMealLastDay().getValue(),
                        meal.getDiscarded(),
                        meal.getUsername().getValue()))
                .collect(Collectors.toList());
    }

    public List<MealResponse> findHistoryMeals(String username) {
        return mealHistoryRepository.findAllMealHistory()
                .stream()
                .filter(meal -> meal.getMealhistoryprimarykey().getUsername().equals(username))
                .map(mealHistory -> mealRepository.findAllMeals(username)
                        .stream()
                        .filter(meal -> mealHistory.getMealhistoryprimarykey().getMealid().equals(meal.getMealId()))
                        .map(meal -> new MealResponse(meal.getMealId(),
                                meal.getMealName().getValue(),
                                meal.getMealType().getValue().toString(),
                                meal.getMealPeriodicity().getValue(),
                                meal.getMealLastDay().getValue(),
                                meal.getDiscarded(),
                                meal.getUsername().getValue()))
                        .collect(Collectors.toList())
                        .get(0))
                .collect(Collectors.toList());
    }

    public List<MealResponse> findHistoryMealsPageable(String username) {
        return mealHistoryRepository.findAllPageableMealHistory(username)
                .stream()
                .filter(meal -> meal.getUsername().getValue().equals(username))
                .map(meal -> new MealResponse(
                        meal.getMealId(),
                        meal.getMealName(),
                        meal.getMealLastDay().getValue(),
                        meal.getUsername().getValue()
                ))
                .collect(Collectors.toList());
    }

    public List<MealResponse> findHistoryMealByNameAndLastDayPageable(MealRequest mealRequest, MealLastDayRequest mealLastDayRequest) {
        return mealHistoryRepository.findHistoryMealByNameAndLastDayPageable(
                        new MealNameRequest(mealRequest.getMealName(), mealRequest.getUsername()),
                        new MealLastDayRequest(mealLastDayRequest.getMeallastday(), mealRequest.getUsername()))
                .stream()
                .map(meal -> new MealResponse(meal.getMealId(),
                        meal.getMealName(),
                        meal.getMealLastDay().getValue(),
                        meal.getUsername().getValue()))
                .collect(Collectors.toList());
    }
}
