package com.mealsapp.mealsservice.application;

import com.mealsapp.mealsservice.domain.MealUser;
import com.mealsapp.mealsservice.infrastructure.MealLoginRequest;
import com.mealsapp.mealsservice.infrastructure.MealLoginResponse;
import com.mealsapp.mealsservice.persistence.MealLoginRepository;
import org.springframework.stereotype.Service;

@Service
public class MealLogin {

    private final MealLoginRepository mealLoginRepository;

    public MealLogin(MealLoginRepository mealLoginRepository) {
        this.mealLoginRepository = mealLoginRepository;
    }

    public MealLoginRepository getMealLoginRepository() {
        return mealLoginRepository;
    }

    public MealLoginResponse findUser(MealLoginRequest mealLoginRequest) {
        MealUser userFound = mealLoginRepository.findUser(mealLoginRequest);

        if (userFound == null) return null;
        return new MealLoginResponse(
                userFound.getUserName().getValue(),
                userFound.getActivated()
        );
    }
}
