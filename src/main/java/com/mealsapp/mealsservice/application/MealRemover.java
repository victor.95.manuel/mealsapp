package com.mealsapp.mealsservice.application;

import com.mealsapp.mealsservice.domain.Meal;
import com.mealsapp.mealsservice.infrastructure.MealRemoveRequest;
import com.mealsapp.mealsservice.persistence.MealRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MealRemover {

    public static final String LUNCH = "LUNCH";
    public static final String DINNER = "DINNER";
    private final MealRepository mealRepository;

    public MealRemover(MealRepository mealRepository) {
        this.mealRepository = mealRepository;
    }

    public void remove(MealRemoveRequest mealRemoveRequest) {
        List<Meal> mealToRemove = mealRepository.findAllMeals(mealRemoveRequest.getUsername())
                .stream()
                .filter(meal -> meal.getUsername().getValue().equals(mealRemoveRequest.getUsername()))
                .filter(meal -> meal.getMealId().equals(mealRemoveRequest.getMealid()))
                .collect(Collectors.toList());

        if (mealToRemove.isEmpty()) return;
        mealRepository.remove(mealToRemove.get(0));
    }

}
