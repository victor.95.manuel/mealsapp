package com.mealsapp.mealsservice.application;

import com.mealsapp.mealsservice.domain.Meal;
import com.mealsapp.mealsservice.infrastructure.MealResetRequest;
import com.mealsapp.mealsservice.persistence.MealRepository;
import org.springframework.stereotype.Service;

@Service
public class MealResetter {

    private final MealRepository mealRepository;

    public MealResetter(MealRepository mealRepository) {
        this.mealRepository = mealRepository;
    }

    public void resetMeal(MealResetRequest mealResetRequest) {
        Meal mealToReset = mapMeal(mealResetRequest);

        if (mealToReset == null) return;
        mealRepository.save(mealToReset);
    }

    private Meal mapMeal(MealResetRequest mealResetRequest) {
        Meal mealToUpdate = mealRepository.findMealToReset(mealResetRequest);

        if (mealToUpdate == null) return null;

        return new Meal(
                mealToUpdate.getMealId(),
                mealToUpdate.getMealName(),
                mealToUpdate.getMealType(),
                mealToUpdate.getMealPeriodicity(),
                null,
                mealToUpdate.getDiscarded(),
                mealToUpdate.getUsername()
        );
    }
}
