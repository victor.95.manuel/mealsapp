package com.mealsapp.mealsservice.domain;

public class MealLoginUserName {

    private final String userName;

    public MealLoginUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }
}
