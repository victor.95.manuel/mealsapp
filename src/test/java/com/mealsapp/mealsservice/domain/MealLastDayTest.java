//package com.mealsapp.mealsservice.domain;
//
//import org.junit.jupiter.api.Test;
//
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//
//class MealLastDayTest {
//
//    @
//    @Test
//    public void test1() {
//        try {
//            MealLastDay mealLastDay = new MealLastDay(new SimpleDateFormat("yyyy-MM-dd").parse("2021-06-09"));
//            assertEquals(0, mealLastDay.getAmountOfDaysFromLastEatenDay());
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//    }
//
//    @Test
//    public void test2() {
//        try {
//            MealLastDay mealLastDay = new MealLastDay(new SimpleDateFormat("yyyy-MM-dd").parse("2021-06-06"));
//            assertEquals(3, mealLastDay.getAmountOfDaysFromLastEatenDay());
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//    }
//
//}